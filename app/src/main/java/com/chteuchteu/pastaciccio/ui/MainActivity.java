package com.chteuchteu.pastaciccio.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.chteuchteu.pastaciccio.R;
import com.chteuchteu.pastaciccio.ThisIsPasta;
import com.chteuchteu.pastaciccio.hlpr.Util;
import com.chteuchteu.pastaciccio.obj.Command;

import java.text.DecimalFormat;

public class MainActivity extends ActionBarActivity {
	private MenuItem menu_refresh;
	private MenuItem menu_submit;
	private MenuItem menu_summary;
    private MenuItem menu_calculator;
	private Context context;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
		
		this.context = this;

        TabsAdapter tabsAdapter = new TabsAdapter(getSupportFragmentManager(), this);
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(tabsAdapter);

        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setViewPager(viewPager);
        tabs.setTextColor(Color.WHITE);
        tabs.setDividerColor(Color.TRANSPARENT);
        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                float p = position + positionOffset;

                if (menu_submit != null)
                    setMenuItemAlpha(R.id.submit, 1 - p);
                if (menu_summary != null)
                    setMenuItemAlpha(R.id.summary, p);
                if (menu_calculator != null)
                    setMenuItemAlpha(R.id.calculator, p);
            }
            @Override
            public void onPageSelected(int position) {
                if (menu_submit != null)
                    menu_submit.setVisible(position == 0);
                if (menu_summary != null)
                    menu_summary.setVisible(position == 1);
                if (menu_calculator != null)
                    menu_calculator.setVisible(position == 1);
            }
            @Override public void onPageScrollStateChanged(int state) { }
        });
        Util.Fonts.setFont(context, (ViewGroup) tabs.getRootView(), Util.Fonts.CustomFont.Roboto_Regular);
		
		// The sauces list and command will be fetched when the menu will be
		// created so the refreshMenuItem won't be null
	}
	
	private void playSound(int raw) {
        MediaPlayer mediaPlayer = MediaPlayer.create(context, raw);
		mediaPlayer.setOnCompletionListener(new OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.release();
            }
        });
		mediaPlayer.start();
	}
	
	private void displayNameDialog() {
		playSound(R.raw.saymyname);
		
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		
		alert.setTitle(getString(R.string.provide_name));

        ContextThemeWrapper ctw = new ContextThemeWrapper(this, R.style.AppTheme);
		final EditText input = new EditText(ctw);
        Util.setMargins(input, 20, 10);
		input.setText(Util.getPref(context, "name"));
		alert.setView(input);
		
		alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String value = input.getText().toString();
                Util.setPref(context, "name", value);
                playSound(R.raw.goddamnright);
            }
        });
		alert.setNegativeButton("Cancel", null);
		
		alert.show();
	}
	
	private void displayCalculatorPopup() {
		Command command = ThisIsPasta.getInstance(context).getCommand();
		if (command == null || command.getOrders().size() == 0)
			return;

        ContextThemeWrapper ctw = new ContextThemeWrapper(this, R.style.AppTheme);

        View view = LayoutInflater.from(context).inflate(R.layout.dialog_calculator, null);

        // Price
        float price_res = command.getPrice();
        TextView price = (TextView) view.findViewById(R.id.sum);
        Util.Fonts.setFont(this, price, Util.Fonts.CustomFont.Roboto_Regular);
        price.setText(price_res + "€");

        // Tickets
        float[] tickets_res = command.getTicketsAmount(price_res);
        TextView tickets = (TextView) view.findViewById(R.id.tickets);
        Util.Fonts.setFont(this, tickets, Util.Fonts.CustomFont.Roboto_Regular);
        tickets.setText(String.valueOf((int) tickets_res[0]));

        TextView tickets_rest = (TextView) view.findViewById(R.id.tickets_rest);
        Util.Fonts.setFont(this, tickets_rest, Util.Fonts.CustomFont.Roboto_Regular);
        DecimalFormat df = new DecimalFormat("###.##");
        tickets_rest.setText(df.format(tickets_res[1]) + "€");

        new AlertDialog.Builder(ctw)
                .setView(view)
                .setPositiveButton("Ok", null)
                .show();
	}

    private void displaySummaryPopup() {
        Command command = ThisIsPasta.getInstance(context).getCommand();
        if (command == null || command.getOrders().size() == 0)
            return;

        ContextThemeWrapper ctw = new ContextThemeWrapper(this, R.style.AppTheme);

        EditText editText = new EditText(ctw);
        Util.setMargins(editText, 20, 10);
        editText.setTextSize(20);
        editText.setText(command.getSummary());

        new AlertDialog.Builder(ctw)
                .setView(editText)
                .setPositiveButton("Ok", null)
                .show();
    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		menu_refresh = menu.findItem(R.id.refresh);
		menu_submit = menu.findItem(R.id.submit);
		menu_summary = menu.findItem(R.id.summary);
        menu_calculator = menu.findItem(R.id.calculator);

		
		ThisIsPasta.getInstance(context).fetchAll(menu_refresh);
		return true;
	}

    private void setMenuItemAlpha(int resId, float alpha) {
        View view = findViewById(resId);
        if (view == null)
            return;
        view.setAlpha(alpha);
    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.refresh:
				ThisIsPasta.getInstance(context).fetchAll(menu_refresh);
				return true;
			case R.id.summary:
				displaySummaryPopup();
                return true;
			case R.id.submit:
				ThisIsPasta.getInstance(context).sendOrder();
                return true;
			case R.id.provide_name:
				displayNameDialog();
                return true;
            case R.id.calculator:
                displayCalculatorPopup();
                return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
