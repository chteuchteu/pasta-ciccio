package com.chteuchteu.pastaciccio.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.chteuchteu.pastaciccio.adptr.CommandsAdapter;
import com.chteuchteu.pastaciccio.R;
import com.chteuchteu.pastaciccio.ThisIsPasta;
import com.chteuchteu.pastaciccio.hlpr.Util;
import com.chteuchteu.pastaciccio.hlpr.Util.Fonts.CustomFont;
import com.chteuchteu.pastaciccio.obj.Command;

public class SummaryFragment extends Fragment {
	private static ListView commandsList;
	private static TextView no_command;
	public static Activity activity;
	
	public SummaryFragment() { }
	
	public static SummaryFragment newInstance() {
        return new SummaryFragment();
	}
	
	@Override
	public void onStart() {
		super.onStart();
	}
	
	@Override
	public void onAttach(Activity act) {
		super.onAttach(act);
		activity = act;
	}
	
	public static void populateCommand() {
		Command todaysCommand = ThisIsPasta.getInstance(activity).getCommand();
		if (todaysCommand != null) {
			no_command.setVisibility(View.GONE);
			CommandsAdapter arrayAdapter = new CommandsAdapter(activity, android.R.layout.simple_list_item_1, todaysCommand.getOrders());
			commandsList.setAdapter(arrayAdapter);
			commandsList.setOnItemClickListener(new OnItemClickListener() {
				@SuppressLint("NewApi")
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    view.setAlpha(view.getAlpha() == 1f ? 0.3f : 1f);
				}
			});
		} else { // There's no command today
			Util.Fonts.setFont(activity, no_command, CustomFont.Roboto_Regular);
			no_command.setVisibility(View.VISIBLE);
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_summary, container, false);
		commandsList = (ListView) rootView.findViewById(R.id.commands_list);
		no_command = (TextView) rootView.findViewById(R.id.nocommand);
		return rootView;
	}
}
