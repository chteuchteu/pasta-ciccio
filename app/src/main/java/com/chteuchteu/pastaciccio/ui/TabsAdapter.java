package com.chteuchteu.pastaciccio.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.chteuchteu.pastaciccio.R;
import com.chteuchteu.pastaciccio.ThisIsPasta;

import java.util.Locale;

public class TabsAdapter extends FragmentStatePagerAdapter {
    private MainActivity mainActivity;

    public TabsAdapter(FragmentManager fm, MainActivity activity) {
        super(fm);
        this.mainActivity = activity;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                MyCommandFragment fragment = MyCommandFragment.newInstance(mainActivity);
                ThisIsPasta.getInstance(mainActivity).commandFragment = fragment;
                return fragment;
            case 1:
                return SummaryFragment.newInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Locale l = Locale.getDefault();
        switch (position) {
            case 0:
                return mainActivity.getString(R.string.my_command).toUpperCase(l);
            case 1:
                return mainActivity.getString(R.string.global_command).toUpperCase(l);
        }
        return null;
    }
}
