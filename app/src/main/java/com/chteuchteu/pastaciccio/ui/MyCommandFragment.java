package com.chteuchteu.pastaciccio.ui;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.chteuchteu.pastaciccio.Enums.Pasta;
import com.chteuchteu.pastaciccio.Enums.Size;
import com.chteuchteu.pastaciccio.Enums.Topping;
import com.chteuchteu.pastaciccio.R;
import com.chteuchteu.pastaciccio.ThisIsPasta;
import com.chteuchteu.pastaciccio.obj.Order;
import com.chteuchteu.pastaciccio.obj.Sauce;

public class MyCommandFragment extends Fragment {
	private Context context;
	
	private RadioButton rb_penne;
	private RadioButton rb_farfalle;
	private RadioButton rb_fusilli;
	private RadioButton rb_spaghetti;
    private RadioButton rb_small;
    private RadioButton rb_medium;
    private RadioButton rb_maxi;
	public Spinner sauce1;
	public Spinner sauce2;
	public Spinner sauce3;
	private TextView tv_sauce2;
	private TextView tv_sauce3;
	private CheckBox cb_gruyere;
	private CheckBox cb_parmesan;
	
	
	public static MyCommandFragment newInstance(Context contextRef) {
		MyCommandFragment fragment = new MyCommandFragment();
		fragment.context = contextRef;
		return fragment;
	}
	
	public MyCommandFragment() { }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_mycommand, container, false);
		
		rb_penne = (RadioButton)rootView.findViewById(R.id.pasta_penne);
		rb_farfalle = (RadioButton)rootView.findViewById(R.id.pasta_farfalle);
		rb_fusilli = (RadioButton)rootView.findViewById(R.id.pasta_fusilli);
		rb_spaghetti = (RadioButton)rootView.findViewById(R.id.pasta_spaghetti);
		rb_small = (RadioButton)rootView.findViewById(R.id.size_small);
		rb_medium = (RadioButton)rootView.findViewById(R.id.size_medium);
		rb_maxi = (RadioButton)rootView.findViewById(R.id.size_maxi);
		sauce1 = (Spinner)rootView.findViewById(R.id.sp_sauce1);
		tv_sauce2 = (TextView)rootView.findViewById(R.id.tv_sauce2);
		sauce2 = (Spinner)rootView.findViewById(R.id.sp_sauce2);
		tv_sauce3 = (TextView)rootView.findViewById(R.id.tv_sauce3);
		sauce3 = (Spinner)rootView.findViewById(R.id.sp_sauce3);
        cb_gruyere = (CheckBox)rootView.findViewById(R.id.tb_gruyere);
        cb_parmesan = (CheckBox)rootView.findViewById(R.id.tb_parmesan);
		
		// Populate sauces lists if needed
		ArrayList<Sauce> sauces = ThisIsPasta.getInstance(context).getSauces();
		if (sauces != null && sauces.size() > 0 && (sauce1.getAdapter() == null || sauce1.getAdapter().getCount() == 0))
			ThisIsPasta.getInstance(context).populateMyCommandSaucesSpinners(sauce1, sauce2, sauce3);

        rb_small.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, getString(R.string.lol_noob), Toast.LENGTH_SHORT).show();
            }
        });
		
		sauce1.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				int visibility = View.VISIBLE;
				if (position == 0)
					visibility = View.GONE;
				
				tv_sauce2.setVisibility(visibility);
				sauce2.setVisibility(visibility);
				
				if (visibility == View.GONE) {
					sauce2.setSelection(0);
					sauce3.setSelection(0);
				}
			}
			@Override public void onNothingSelected(AdapterView<?> parent) { }
		});
		
		sauce2.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				int visibility = View.VISIBLE;
				if (position == 0)
					visibility = View.GONE;
				
				tv_sauce3.setVisibility(visibility);
				sauce3.setVisibility(visibility);
				
				if (visibility == View.GONE)
					sauce3.setSelection(0);
			}
			@Override public void onNothingSelected(AdapterView<?> parent) { }
		});
		
		return rootView;
	}
	
	// We assume that we already know the sauces list
	public void updateView() {
		Order myOrder = ThisIsPasta.getInstance(context).getMyOrder();
		if (myOrder == null)
			return;
		ArrayList<Sauce> sauces = ThisIsPasta.getInstance(context).getSauces();
		
		rb_penne.setChecked(myOrder.getPasta() == Pasta.PENNE);
		rb_farfalle.setChecked(myOrder.getPasta() == Pasta.FARFALLE);
		rb_fusilli.setChecked(myOrder.getPasta() == Pasta.FUSILLI);
		rb_spaghetti.setChecked(myOrder.getPasta() == Pasta.SPAGHETTI);

        rb_small.setChecked(myOrder.getSize() == Size.SMALL);
        rb_medium.setChecked(myOrder.getSize() == Size.MEDIUM);
        rb_maxi.setChecked(myOrder.getSize() == Size.MAXI);
		
		switch (myOrder.getSauces().size()) {
			case 0:
				tv_sauce2.setVisibility(View.GONE);
				tv_sauce3.setVisibility(View.GONE);
				sauce2.setVisibility(View.GONE);
				sauce3.setVisibility(View.GONE);
				sauce1.setSelection(0);
				sauce2.setSelection(0);
				sauce3.setSelection(0);
				break;
			case 1:
				tv_sauce2.setVisibility(View.GONE);
				tv_sauce3.setVisibility(View.GONE);
				sauce2.setVisibility(View.GONE);
				sauce3.setVisibility(View.GONE);
				sauce1.setSelection(sauces.indexOf(myOrder.getSauces().get(0))+1);
				sauce2.setSelection(0);
				sauce3.setSelection(0);
				break;
			case 2:
				tv_sauce2.setVisibility(View.VISIBLE);
				tv_sauce3.setVisibility(View.GONE);
				sauce2.setVisibility(View.VISIBLE);
				sauce3.setVisibility(View.GONE);
				sauce1.setSelection(sauces.indexOf(myOrder.getSauces().get(0))+1);
				sauce2.setSelection(sauces.indexOf(myOrder.getSauces().get(1))+1);
				sauce3.setSelection(0);
				break;
			case 3:
				tv_sauce2.setVisibility(View.VISIBLE);
				tv_sauce3.setVisibility(View.VISIBLE);
				sauce2.setVisibility(View.VISIBLE);
				sauce3.setVisibility(View.VISIBLE);
				sauce1.setSelection(sauces.indexOf(myOrder.getSauces().get(0))+1);
				sauce2.setSelection(sauces.indexOf(myOrder.getSauces().get(1))+1);
				sauce3.setSelection(sauces.indexOf(myOrder.getSauces().get(2))+1);
				break;
		}
		
		cb_gruyere.setChecked(myOrder.getToppings().contains(Topping.GRUYÈRE));
		cb_parmesan.setChecked(myOrder.getToppings().contains(Topping.PARMESAN));
	}
	
	public Order getOrderFromView() {
		Order order = new Order();
		order.setPasta(getPastaFromView());
		order.setSize(getSizeFromView());
		
		if (cb_gruyere.isChecked())
			order.addTopping(Topping.GRUYÈRE);
		if (cb_parmesan.isChecked())
			order.addTopping(Topping.PARMESAN);
		
		for (Sauce sauce : getSaucesFromView())
			order.addSauce(sauce);
		
		return order;
	}
	
	private Pasta getPastaFromView() {
		if (rb_penne.isChecked())
			return Pasta.PENNE;
		if (rb_farfalle.isChecked())
			return Pasta.FARFALLE;
		if (rb_fusilli.isChecked())
			return Pasta.FUSILLI;
		if (rb_spaghetti.isChecked())
			return Pasta.SPAGHETTI;
		return null;
	}
	
	private Size getSizeFromView() {
		if (rb_small.isChecked())
            return Size.SMALL;
        if (rb_medium.isChecked())
            return Size.MEDIUM;
        if (rb_maxi.isChecked())
            return Size.MAXI;
        return null;
	}
	
	private ArrayList<Sauce> getSaucesFromView() {
		ArrayList<Sauce> sauces = new ArrayList<>();
		
		if (sauce1.getSelectedItemPosition() != 0)
			sauces.add((Sauce) sauce1.getSelectedItem());
		if (sauce2.getVisibility() == View.VISIBLE && sauce2.getSelectedItemPosition() != 0)
			sauces.add((Sauce) sauce2.getSelectedItem());
		if (sauce3.getVisibility() == View.VISIBLE && sauce3.getSelectedItemPosition() != 0)
			sauces.add((Sauce) sauce3.getSelectedItem());
		
		return sauces;
	}
}
