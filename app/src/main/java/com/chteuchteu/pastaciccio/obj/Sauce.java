package com.chteuchteu.pastaciccio.obj;

import android.content.Context;

import com.chteuchteu.pastaciccio.R;
import com.chteuchteu.pastaciccio.hlpr.Util.WordUtils;

public class Sauce {
	private String name;
	private String prettyName;
	
	/**
	 * Creates a new sauce from its name, and generates its prettyName attribute
	 * @param name (serialized value)
	 */
	public Sauce(String name) {
		this.name = name;
		this.prettyName = generatePrettyName(name);
	}
	
	public static String generatePrettyName(String name) {
		return WordUtils.capitalize(name);
	}
	
	public boolean equals(Sauce sauce) {
		return this.name.equals(sauce.getPrettyName());
	}
	
	public static Sauce noneSauce(Context context) {
		return new Sauce(context != null ? context.getString(R.string.none) : "None");
	}
	
	@Override
	public String toString() { return this.prettyName; }
	
	public String getName() { return this.name; }
	public String getPrettyName() { return this.prettyName; }
}
