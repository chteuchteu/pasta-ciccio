package com.chteuchteu.pastaciccio.obj;

import java.util.ArrayList;

import com.chteuchteu.pastaciccio.Enums.Pasta;
import com.chteuchteu.pastaciccio.Enums.Size;
import com.chteuchteu.pastaciccio.ThisIsPasta;

public class Command {
	private String id;
	private ArrayList<Order> orders;
	
	public Command() {
		this.orders = new ArrayList<>();
	}
	
	public String getSummary() {
		String plainText = "";
		
		if (this.orders.size() == 0)
			return plainText;
		
		for (Pasta pasta : Pasta.values()) {
			for (Size size : Size.values()) {
				// Get the orders with those values
				ArrayList<Order> orders = new ArrayList<>();
				for (Order order : this.orders) {
					if (order.matchesType(pasta, size))
						orders.add(order);
				}
				if (orders.size() > 0)
					plainText += orders.size() + " x " + orders.get(0).getSummaryString() + "\n";
			}
		}
		
		
		return plainText;
	}

    public float getPrice() {
        float price = 0;

        for (Order order : orders)
            price += order.getSize().getPrice();

        return price;
    }

    public float[] getTicketsAmount(float sum) {
        int nbTickets = (int) Math.ceil(sum / ThisIsPasta.TICKET_AMOUNT);
        float rest = ThisIsPasta.TICKET_AMOUNT % sum;
        return new float[] { nbTickets, rest };
    }
	
	public String getId() { return this.id; }
	public void setId(String val) { this.id = val; }
	
	public ArrayList<Order> getOrders() { return this.orders; }
	public void addOrder(Order order) { this.orders.add(order); }
}
