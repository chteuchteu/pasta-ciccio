package com.chteuchteu.pastaciccio.obj;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.chteuchteu.pastaciccio.Enums.Pasta;
import com.chteuchteu.pastaciccio.Enums.Size;
import com.chteuchteu.pastaciccio.Enums.Topping;
import com.chteuchteu.pastaciccio.ThisIsPasta;

public class Order {
	private Size size;
	private Pasta pasta;
	private ArrayList<Sauce> sauces;
	private ArrayList<Topping> toppings;
	
	public Order() {
		this.sauces = new ArrayList<>();
		this.toppings = new ArrayList<>();
	}
	
	public Size getSize() { return this.size; }
	public void setSize(Size val) { this.size = val; }
	
	public Pasta getPasta() { return this.pasta; }
	public void setPasta(Pasta val) { this.pasta = val; }
	
	public ArrayList<Sauce> getSauces() { return this.sauces; }
	public void addSauce(Sauce val) {
		if (!this.sauces.contains(val))
			this.sauces.add(val);
	}
	
	public ArrayList<Topping> getToppings() { return this.toppings; }
	public void addTopping(Topping val) {
		if (!this.toppings.contains(val))
			this.toppings.add(val);
	}
	public String getToppingsText() {
		if (toppings == null || toppings.size() == 0 || toppings.get(0) == null)
			return "";
		switch (toppings.size()) {
			case 1: return this.toppings.get(0).getShortString();
			case 2: return this.toppings.get(0).getShortString() + this.toppings.get(1).getShortString();
			default: return "";
		}
	}
	
	public JSONObject toJSONObject() throws JSONException {
		JSONObject order = new JSONObject();
		order.put("pasta", this.pasta.getSerializedValue());
		order.put("size", this.size.getSerializedValue());
		
		JSONArray sauces = new JSONArray();
		for (Sauce sauce : this.sauces)
			sauces.put(sauce.getName());
		order.put("sauces", sauces);
		
		JSONArray toppings = new JSONArray();
		for (Topping topping : this.toppings)
			toppings.put(topping.getSerializedValue());
		order.put("toppings", toppings);
		
		return order;
	}
	
	public boolean matchesType(Pasta pasta, Size size) {
		return this.pasta == pasta && this.size == size;
	}
	
	public String getSummaryString() {
		return this.size.name() + " " + this.pasta.name();
	}
	
	@Override
	public String toString() {
		return "{" + this.getPasta().name() + "-" + this.getSize().name() + "}";
	}
	
	private int getComparatorScore() {
		int pasta = Arrays.asList(Pasta.values()).indexOf(this.getPasta()) + 1;
		int size = Arrays.asList(Size.values()).indexOf(this.getSize()) + 1;
		return pasta * pasta + size;
	}
	
	public static Order jsonToOrder(Context context, String jsonOrder) throws JSONException {
		return jsonToOrder(context, new JSONObject(jsonOrder));
	}
	
	public static Order jsonToOrder(Context contextRef, JSONObject jsonOrder) throws JSONException {
		Order order = new Order();
		order.setSize(Size.get(jsonOrder.getString("size")));
		order.setPasta(Pasta.get(jsonOrder.getString("pasta")));
		
		JSONArray jsonSauces = jsonOrder.getJSONArray("sauces");
		for (int y=0; y<jsonSauces.length(); y++) {
			Sauce sauce = ThisIsPasta.getInstance(contextRef).getSauce(jsonSauces.get(y).toString());
			order.addSauce(sauce);
		}
		
		JSONArray jsonToppings = jsonOrder.getJSONArray("toppings");
		for (int y=0; y<jsonToppings.length(); y++) {
			Topping topping = Topping.get(jsonToppings.getString(y));
			order.addTopping(topping);
		}
		return order;
	}
	
	public static class OrderComparator implements Comparator<Order> {
		@Override
		public int compare(Order o1, Order o2) {
			return o1.getComparatorScore() - o2.getComparatorScore();
		}
	}
}
