package com.chteuchteu.pastaciccio;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.chteuchteu.pastaciccio.hlpr.Util;
import com.chteuchteu.pastaciccio.obj.Command;
import com.chteuchteu.pastaciccio.obj.Order;
import com.chteuchteu.pastaciccio.obj.Order.OrderComparator;
import com.chteuchteu.pastaciccio.obj.Sauce;
import com.chteuchteu.pastaciccio.ui.MyCommandFragment;
import com.chteuchteu.pastaciccio.ui.SummaryFragment;

public class ThisIsPasta {
    public static final float TICKET_AMOUNT = 8.8f;
	private static ThisIsPasta instance;
	private static final String apiUri = "http://papaciccio.appsdeck.eu/api/";
	
	private Command command;
	private Order myOrder;
	private ArrayList<Sauce> sauces;

    public MyCommandFragment commandFragment;
	
	private Context contextRef;
	
	private ThisIsPasta(Context context) {
		loadInstance(context);
	}
	
	private void loadInstance(Context context) {
		if (context != null)
			this.contextRef = context;
		this.sauces = new ArrayList<>();
		this.command = null;
		
		if (Util.getPref(contextRef, "todaysCommandDate").equals(Util.getApiToday())) {
			// Parse order
			if (!Util.getPref(contextRef, "myOrder").equals("")) {
				try {
					this.myOrder = Order.jsonToOrder(contextRef, Util.getPref(contextRef, "myOrder"));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		} else {
			Util.removePref(contextRef, "todaysCommandDate");
			Util.removePref(contextRef, "myOrder");
			Util.removePref(contextRef, "todaysCommandId");
		}
	}
	
	public static synchronized ThisIsPasta getInstance(Context context) {
		if (instance == null)
			instance = new ThisIsPasta(context);
		return instance;
	}
	
	public Command getCommand() { return this.command; }
	
	/**
	 * Returns a sauce object from the specified serializedName.
	 * If the sauce isn't already in the sauces list, add it
	 */
	public Sauce getSauce(String serializedName) {
		Sauce sauce = new Sauce(serializedName);
		
		// Check if we downloaded it
		for (Sauce s : this.sauces) {
			if (s.equals(sauce))
				return s;
		}
		
		// We haven't downloaded it yet : add it to the sauces
		// list and return it
		this.sauces.add(sauce);
		return sauce;
	}
	
	public void fetchAll(MenuItem menuItemRef) {
		new RefreshAll(menuItemRef).execute();
	}
	
	private boolean fetchCommand() {
		String apiCallUri = apiUri + "commands/" + Util.getApiToday();
		HttpClient httpclient = new DefaultHttpClient();
		String responseBody = "";
		try {
			HttpGet httpget = new HttpGet(apiCallUri);
			
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			responseBody = httpclient.execute(httpget, responseHandler);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			httpclient.getConnectionManager().shutdown();
		}
		
		// Parse JSON
		try {
			JSONObject obj = new JSONObject(responseBody).getJSONObject("command");
			JSONArray jsonOrders = obj.getJSONArray("orders");
			
			Command todaysCommand = new Command();
			for (int i=0; i<jsonOrders.length(); i++) {
				JSONObject jsonOrder = jsonOrders.getJSONObject(i);
				Order order = Order.jsonToOrder(contextRef, jsonOrder);
				todaysCommand.addOrder(order);
			}
			
			Collections.sort(todaysCommand.getOrders(), new OrderComparator());
			command = todaysCommand;
		} catch (JSONException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private void fetchSauces() {
		String apiCallUri = apiUri + "sauces";
		
		HttpClient httpclient = new DefaultHttpClient();
		String responseBody = "";
		try {
			HttpGet httpget = new HttpGet(apiCallUri);
			
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			responseBody = httpclient.execute(httpget, responseHandler);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			httpclient.getConnectionManager().shutdown();
		}
		
		// Parse JSON
		try {
			JSONArray obj = new JSONArray(responseBody);
			
			this.sauces.clear();
			
			this.sauces.add(Sauce.noneSauce(contextRef));
			for (int i=0; i<obj.length(); i++) {
				// If not already, the sauce is automatically added into the sauces list
				getSauce(obj.get(i).toString());
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void sendOrder() {
		new OrderSender().execute();
	}
	
	private boolean sendMyOrder(Order myOrder) {
		String apiCallUri = apiUri + "commands/" + Util.getApiToday();
		
		boolean ok = true;
		if (this.myOrder != null)
			ok = deleteOrder(this.myOrder);
		
		if (ok) {
			this.myOrder = myOrder;
			
			HttpClient httpClient;
			HttpPost httpPost;
			InputStream inStream = null;
			
			try {
				httpClient = new DefaultHttpClient();
				httpPost = new HttpPost(apiCallUri);
				HttpEntity postEntity = new ByteArrayEntity(this.myOrder.toJSONObject().toString().getBytes("UTF-8"));
				httpPost.setEntity(postEntity);
				
				// Execute and get the response
				HttpResponse response = httpClient.execute(httpPost);
				HttpEntity entity = response.getEntity();
				
				if (entity != null) {
					inStream = entity.getContent();
					try {
						String serverResponse = Util.Streams.convertStreamtoString(inStream);
						Log.v("serverResponse", serverResponse);
						// Check server's response
						JSONObject obj = new JSONObject(serverResponse);
						ok = !obj.getBoolean("error");
						// Command id
						String id = obj.getJSONObject("command").getString("id");
						this.command.setId(id);
						Util.setPref(contextRef, "todaysCommandId", id);
						Util.setPref(contextRef, "todaysCommandDate", Util.getApiToday());
						Util.setPref(contextRef, "myOrder", this.myOrder.toJSONObject().toString());
					} finally {
						inStream.close();
					}
				}
			} catch (IOException | JSONException exception) {
				exception.printStackTrace();
				ok = false;
			} finally {
				if (inStream != null) {
					try {
						inStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
			ok = true;
		} else
			ok = false;
		
		return ok;
	}
	
	private boolean deleteOrder(Order order) {
		if (order == null)
			return true;
		
		String apiCallUri = apiUri + "commands/" + Util.getApiToday();
		
		URL url;
		try {
			url = new URL(apiCallUri);
		} catch (MalformedURLException exception) {
			exception.printStackTrace();
            return false;
		}
		HttpURLConnection httpURLConnection = null;
		try {
			httpURLConnection = (HttpURLConnection) url.openConnection();
			httpURLConnection.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");
			httpURLConnection.setRequestMethod("DELETE");
			httpURLConnection.addRequestProperty("order", myOrder.toJSONObject().toString());
			System.out.println(httpURLConnection.getResponseCode());
		} catch (NullPointerException | IOException | JSONException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (httpURLConnection != null)
				httpURLConnection.disconnect();
		}
		
		return true;
	}
	
	private class RefreshAll extends AsyncTask<Void, Void, Void> {
		private MenuItem menuItemRef;
		private Handler mHandler;
		
		public RefreshAll(MenuItem menuItemRef) {
			this.menuItemRef = menuItemRef;
			this.mHandler = new Handler();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			mHandler.post(new Runnable() {
				@Override
				public void run() {
					menuItemRef.setTitle(contextRef.getString(R.string.refresh_sauces));
				}
			});
			fetchSauces();
			mHandler.post(new Runnable() {
				@Override
				public void run() {
					menuItemRef.setTitle(contextRef.getString(R.string.refresh_command));
					populateMyCommandSaucesSpinners();
					if (myOrder != null)
						commandFragment.updateView();
				}
			});
			fetchCommand();
			mHandler.post(new Runnable() {
				@Override
				public void run() {
					menuItemRef.setTitle(contextRef.getString(R.string.refresh));
				}
			});
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void res) {
			super.onPostExecute(res);
			
			SummaryFragment.populateCommand();
		}
	}
	
	private class OrderSender extends AsyncTask<Void, Void, Void> {
		private boolean success;
		
		@Override
		protected Void doInBackground(Void... params) {
			success = sendMyOrder(commandFragment.getOrderFromView());
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void res) {
			super.onPostExecute(res);
			
			if (success)
				Toast.makeText(contextRef, contextRef.getString(R.string.success_submit), Toast.LENGTH_SHORT).show();
			else
				Toast.makeText(contextRef, contextRef.getString(R.string.error_submit), Toast.LENGTH_SHORT).show();
		}
	}
	
	private boolean populateMyCommandSaucesSpinners() {
		return populateMyCommandSaucesSpinners(null, null, null);
	}
	
	public boolean populateMyCommandSaucesSpinners(Spinner sauce1, Spinner sauce2, Spinner sauce3) {
		if (this.sauces.size() == 0)
			return false;
		
		if (sauce1 == null)
			sauce1 = commandFragment.sauce1;
		if (sauce2 == null)
			sauce2 = commandFragment.sauce2;
		if (sauce3 == null)
			sauce3 = commandFragment.sauce3;
		
		int sauce1SelectedIndex = sauce1.getSelectedItemPosition();
		int sauce2SelectedIndex = sauce2.getSelectedItemPosition();
		int sauce3SelectedIndex = sauce3.getSelectedItemPosition();
		
		ArrayAdapter<Sauce> saucesAdapter = new ArrayAdapter<>(contextRef, android.R.layout.simple_spinner_item, this.sauces);
		saucesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sauce1.setAdapter(saucesAdapter);
		sauce1.setSelection(sauce1SelectedIndex);
		sauce2.setAdapter(saucesAdapter);
		sauce2.setSelection(sauce2SelectedIndex);
		sauce3.setAdapter(saucesAdapter);
		sauce3.setSelection(sauce3SelectedIndex);
		
		return true;
	}
	
	public ArrayList<Sauce> getSauces() { return this.sauces; }
	public Order getMyOrder() { return this.myOrder; }
}
