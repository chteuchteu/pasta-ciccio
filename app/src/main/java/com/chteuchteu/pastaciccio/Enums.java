package com.chteuchteu.pastaciccio;

import android.annotation.SuppressLint;

@SuppressLint("DefaultLocale")
public class Enums {
	public static enum Size {
		SMALL("Petit", 4.5f), MEDIUM("Normal", 5.5f), MAXI("Maxi", 7.5f);
		String displayTxt;
        float price;
		Size(String displayTxt, float price) {
            this.displayTxt = displayTxt;
            this.price = price;
        }
		public String getDisplayText() { return this.displayTxt; }
		public String getSerializedValue() { return this.name().toLowerCase(); }
        public float getPrice() { return this.price; }
		public static Size get(String serializedValue) {
			for (Size size : Size.values()) {
				if (size.getSerializedValue().equals(serializedValue))
					return size;
			}
			return defaultValue();
		}
		public static Size defaultValue() { return Size.MEDIUM; }
	}
	
	public static enum Pasta {
		PENNE("Penne"), FUSILLI("Fusilli"), FARFALLE("Farfalle"), SPAGHETTI("Spaghetti");
		String displayTxt;
		Pasta(String displayTxt) { this.displayTxt = displayTxt; }
		public String getSerializedValue() { return this.name().toLowerCase(); }
        public String getDisplayText() { return this.displayTxt; }
		public static Pasta get(String serializedValue) {
			for (Pasta pasta : Pasta.values()) {
				if (pasta.getSerializedValue().equals(serializedValue))
					return pasta;
			}
			return defaultValue();
		}
		public static Pasta defaultValue() { return Pasta.FUSILLI; }
	}
	
	public static enum Topping {
		GRUYÈRE, PARMESAN;
		public String getShortString() { return this.name().substring(0, 1); }
		public String getSerializedValue() { return this.name().toLowerCase(); }
		public static Topping get(String serializedValue) {
			for (Topping topping : Topping.values()) {
				if (topping.getSerializedValue().equals(serializedValue))
					return topping;
			}
			return null;
		}
	}
}
