package com.chteuchteu.pastaciccio.adptr;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.chteuchteu.pastaciccio.R;
import com.chteuchteu.pastaciccio.hlpr.Util;
import com.chteuchteu.pastaciccio.hlpr.Util.Fonts.CustomFont;
import com.chteuchteu.pastaciccio.obj.Order;

public class CommandsAdapter extends ArrayAdapter<Order> {
	private Context c;
	
	public CommandsAdapter(Context context, int resource, List<Order> items) {
		super(context, resource, items);
		this.c = context;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		
		if (v == null) {
			LayoutInflater vi = LayoutInflater.from(getContext());
			v = vi.inflate(R.layout.command_item, parent, false);
		}
		
		Order o = getItem(position);
		
		if (o != null) {
			TextView line1 = (TextView) v.findViewById(R.id.line1);
			TextView sauce1 = (TextView) v.findViewById(R.id.sauce1);
			TextView sauce2 = (TextView) v.findViewById(R.id.sauce2);
			TextView sauce3 = (TextView) v.findViewById(R.id.sauce3);
			TextView toppings = (TextView) v.findViewById(R.id.toppings);
			
			line1.setText(o.getSize().getDisplayText() + " " + o.getPasta().getDisplayText());
			
			switch (o.getSauces().size()) {
				case 0:
					sauce1.setVisibility(View.GONE);
					sauce2.setVisibility(View.GONE);
					sauce3.setVisibility(View.GONE);
					break;
				case 1:
					sauce1.setText(o.getSauces().get(0).getPrettyName());
					sauce2.setVisibility(View.GONE);
					sauce3.setVisibility(View.GONE);
					break;
				case 2:
					sauce1.setText(o.getSauces().get(0).getPrettyName());
					sauce2.setText(o.getSauces().get(1).getPrettyName());
					sauce3.setVisibility(View.GONE);
					break;
				case 3:
					sauce1.setText(o.getSauces().get(0).getPrettyName());
					sauce2.setText(o.getSauces().get(1).getPrettyName());
					sauce3.setText(o.getSauces().get(2).getPrettyName());
					break;
			}
			
			toppings.setText(o.getToppingsText());
			
			Util.Fonts.setFont(c, line1, CustomFont.RobotoCondensed_Regular);
			Util.Fonts.setFont(c, sauce1, CustomFont.Roboto_Regular);
			Util.Fonts.setFont(c, sauce2, CustomFont.Roboto_Regular);
			Util.Fonts.setFont(c, sauce3, CustomFont.Roboto_Regular);
			Util.Fonts.setFont(c, toppings, CustomFont.Roboto_Regular);
		}
		
		return v;
	}
}
