package com.chteuchteu.pastaciccio.hlpr;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class Util {
	public static final class Fonts {
		/* ENUM Custom Fonts */
		public enum CustomFont {
			RobotoCondensed_Regular("RobotoCondensed-Regular.ttf"), Roboto_Regular("Roboto-Regular.ttf");
			final String file;
			private CustomFont(String fileName) { this.file = fileName; }
			public String getValue() { return this.file; }
		}
		
		/* Fonts */
		public static void setFont(Context c, ViewGroup g, CustomFont font) {
			Typeface mFont = Typeface.createFromAsset(c.getAssets(), font.getValue());
			setFont(g, mFont);
		}
		
		public static void setFont(Context c, TextView t, CustomFont font) {
			Typeface mFont = Typeface.createFromAsset(c.getAssets(), font.getValue());
			t.setTypeface(mFont);
		}
		
		public static void setFont(Context c, Button t, CustomFont font) {
			Typeface mFont = Typeface.createFromAsset(c.getAssets(), font.getValue());
			t.setTypeface(mFont);
		}
		
		private static void setFont(ViewGroup group, Typeface font) {
			int count = group.getChildCount();
			View v;
			for (int i = 0; i < count; i++) {
				v = group.getChildAt(i);
				if (v instanceof TextView) {
					((TextView) v).setTypeface(font);
				} else if (v instanceof ViewGroup)
					setFont((ViewGroup) v, font);
			}
		}
	}
	
	@SuppressLint("SimpleDateFormat")
	public static String getApiToday() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(cal.getTime());
	}
	
	public static String getPref(Context c, String key) {
		return c.getSharedPreferences("user_pref", Context.MODE_PRIVATE).getString(key, "");
	}
	
	public static void setPref(Context c, String key, String value) {
		if (value.equals(""))
			removePref(c, key);
		else {
			SharedPreferences prefs = c.getSharedPreferences("user_pref", Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = prefs.edit();
			editor.putString(key, value);
			editor.apply();
		}
	}
	
	public static void removePref(Context c, String key) {
		SharedPreferences prefs = c.getSharedPreferences("user_pref", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		editor.remove(key);
		editor.apply();
	}
	
	@SuppressLint("DefaultLocale")
	public static final class WordUtils {
		public static String capitalize(String str) {
			return capitalize(str, null);
		}
		
		private static String capitalize(String str, char[] delimiters) {
			int delimLen = (delimiters == null ? -1 : delimiters.length);
			if (str == null || str.length() == 0 || delimLen == 0) {
				return str;
			}
			int strLen = str.length();
			StringBuilder buffer = new StringBuilder(strLen);
			boolean capitalizeNext = true;
			for (int i = 0; i < strLen; i++) {
				char ch = str.charAt(i);
				
				if (isDelimiter(ch, delimiters)) {
					buffer.append(ch);
					capitalizeNext = true;
				} else if (capitalizeNext) {
					buffer.append(Character.toTitleCase(ch));
					capitalizeNext = false;
				} else {
					buffer.append(ch);
				}
			}
			return buffer.toString();
		}
		
		private static boolean isDelimiter(char ch, char[] delimiters) {
			if (delimiters == null) {
				return Character.isWhitespace(ch);
			}
            for (char delimiter : delimiters) {
                if (ch == delimiter)
                    return true;
            }
			return false;
		}
	}
	
	public static final class Streams {
		public static String convertStreamtoString(InputStream is) {
			Scanner s = new Scanner(is);
			s.useDelimiter("\\A");
			String ret = s.hasNext() ? s.next() : "";
			s.close();
			return ret;
		}
	}

    public static void setMargin(View v, int m) { setMargins(v, m, m, m, m); }
    public static void setMargins(View view, int v, int h) { setMargins(view, h, v, h, v); }

    public static void setMargins(View v, int l, int t, int r, int b) {
        if (v.getLayoutParams() == null)
            v.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(l, t, r, b);
            v.requestLayout();
        }
    }
}
